package com.infy.utility.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class LogDataEntity {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
	
	private String attrConnID;
	
	private String attrCallUUID;
	
	private String callID;
	
	private String originalDialPlanDigit;

	public LogDataEntity() {}
	
	public LogDataEntity(String attrConnID, String attrCallUUID, String callID, String originalDialPlanDigit) {
		super();
		this.attrConnID = attrConnID;
		this.attrCallUUID = attrCallUUID;
		this.callID = callID;
		this.originalDialPlanDigit = originalDialPlanDigit;
	}

	public String getAttrConnID() {
		return attrConnID;
	}

	public void setAttrConnID(String attrConnID) {
		this.attrConnID = attrConnID;
	}

	public String getAttrCallUUID() {
		return attrCallUUID;
	}

	public void setAttrCallUUID(String attrCallUUID) {
		this.attrCallUUID = attrCallUUID;
	}

	public String getCallID() {
		return callID;
	}

	public void setCallID(String callID) {
		this.callID = callID;
	}

	public String getOriginalDialPlanDigit() {
		return originalDialPlanDigit;
	}

	public void setOriginalDialPlanDigit(String originalDialPlanDigit) {
		this.originalDialPlanDigit = originalDialPlanDigit;
	}

	@Override
	public String toString() {
		return "LogData [id=" + id + ", attrConnID=" + attrConnID + ", attrCallUUID=" + attrCallUUID + ", callID="
				+ callID + ", originalDialPlanDigit=" + originalDialPlanDigit + "]";
	}
	
	
}
