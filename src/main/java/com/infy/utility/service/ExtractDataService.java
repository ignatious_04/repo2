package com.infy.utility.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.infy.utility.entity.LogDataEntity;
import com.infy.utility.repository.LogDataRepository;

@Service
public class ExtractDataService {
	
	private static Pattern PAT_WHITE_SPACE = Pattern.compile("\\s");
	private static Pattern PAT_EVENT_REQUEST = Pattern.compile(".*message ?EventRouteRequest$");
	
	@Autowired
	Environment env;
	
	@Autowired
	LogDataRepository repo;
	
	@Value("${log-files-path}")
	private String path;
	
	@Value("${output-file-name}")
	private String outputFileName;
	
	public void execute() {
		try {
			if(path==null || path.isEmpty()) {
				System.out.println("Path provided is NUll or empty");
			}else {
				System.out.println("Path ="+path);
				File dir = new File(path);
				if(dir.isDirectory()) {
					readAndProcessFiles(dir);
					writeDataToFile();
				}else {
					System.out.println("Path provided is Invalid");
				}
				
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
	}


	private void writeDataToFile() {

		if(outputFileName==null || outputFileName.isEmpty()) {
			outputFileName = "output.csv";
		}
		try (FileOutputStream outputStream = new FileOutputStream(outputFileName)){
			
			repo.findAll().forEach(lde ->{
				String out = String.format("%s,%s,%s,%s\n",lde.getAttrConnID(),lde.getAttrCallUUID(),lde.getCallID(),lde.getOriginalDialPlanDigit());
				try {
					outputStream.write(out.getBytes());
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}


	private void readAndProcessFiles(File dir) {
			File[] filesArray = dir.listFiles();
			for (File fileEntry : filesArray) {
				try{
					if (fileEntry.isDirectory()) {
						readAndProcessFiles(fileEntry);
					}
					else {
						System.out.println("Processing file:"+fileEntry.getName());
//						long start = System.nanoTime();
						processFile(fileEntry);
//						long end = System.nanoTime();
//						System.out.println("Time Taken="+(end-start));
					}
				}catch (Exception e) {
					e.printStackTrace();
				}
			}		
	}


	private void processFile(File file) {
		
		try(BufferedReader br = new BufferedReader(new FileReader(file))){
			
			String line;
			String out;
			LogDataEntity lde =null;
			
	        while ((line = br.readLine()) != null) {
	        	line = line.replaceAll("\\s", "");
	        	line = PAT_WHITE_SPACE.matcher(line).replaceAll("");
	        	if(PAT_EVENT_REQUEST.matcher(line).matches()) {
	        		if(lde!=null) {
	        			repo.save(lde);
	        		}
	        		lde = new LogDataEntity();
	        	}
	        	if(lde!=null) {
	        		if(line.contains("AttributeConnID")) {
		        		out = line.substring("AttributeConnID".length());
		        		lde.setAttrConnID(out);
		        	}else if(line.contains("AttributeCallUUID")) {
		        		if(lde!=null) {
		        			out = line.substring("AttributeCallUUID".length());
		        			out = out.substring(1, out.length()-1);
		        			lde.setAttrCallUUID(out);
		        		}
		        	}else if(line.contains("'Call-ID'")) {
		        		if(lde!=null) {
		        			out = line.substring("'Call-ID'".length());
		        			out = out.substring(1, out.length()-1);
		        			lde.setCallID(out);
		        		}
		        	}else if(line.contains("'original-dialplan-digits'")) {
		        		if(lde!=null) {
		        			out = line.substring("'original-dialplan-digits'".length());
		        			out = out.substring(1, out.length()-1);
		        			lde.setOriginalDialPlanDigit(out);
		        		}
		        	}else if(line.contains("AttributeEventSequenceNumber")) {
		        		if(lde!=null){
		        			repo.save(lde);
		        			lde=null;
		        		}
		        	}
	        	}
	        }
		}catch(Exception e) {
			e.printStackTrace();
		}
        
	}

}
