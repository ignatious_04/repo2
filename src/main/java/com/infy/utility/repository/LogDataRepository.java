package com.infy.utility.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.infy.utility.entity.LogDataEntity;

@Repository
public interface LogDataRepository extends CrudRepository<LogDataEntity, Long> {

}
