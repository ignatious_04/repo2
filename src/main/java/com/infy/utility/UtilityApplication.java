package com.infy.utility;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.infy.utility.service.ExtractDataService;

@SpringBootApplication
public class UtilityApplication implements CommandLineRunner {

	@Autowired
	ExtractDataService extractData;
	
	public static void main(String[] args) {
		SpringApplication.run(UtilityApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		extractData.execute();
	}

}
